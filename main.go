package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"reflect"
	"sync"
	"time"

	maelstrom "github.com/jepsen-io/maelstrom/demo/go"
	"github.com/oklog/ulid/v2"
)

type App struct {
	n             *maelstrom.Node
	neighborNodes []string
	logFile       *os.File

	messages map[int64]bool
	msgMutex sync.RWMutex
}

func main() {
	node := maelstrom.NewNode()
	application := &App{
		n:        node,
		messages: map[int64]bool{},
	}

	if err := application.setupLogFile(); err != nil {
		log.Fatal(err)
		return
	}

	application.logPrintln("Logging to the file ", application.logFile.Name())

	application.n.Handle("echo", application.handleEchoMessage)
	application.n.Handle("generate", application.handleGenerateMessage)
	application.n.Handle("broadcast", application.handleBroadcastMessage)
	application.n.Handle("read", application.handleReadMessage)
	application.n.Handle("topology", application.handleTopologyMessage)

	if err := application.n.Run(); err != nil {
		log.Fatal(err)
	}
}

func (a *App) handleEchoMessage(msg maelstrom.Message) error {
	body, err := readMessageBody(msg)
	if err != nil {
		return err
	}

	body["type"] = "echo_ok"
	return a.n.Reply(msg, body)
}

func (a *App) handleGenerateMessage(msg maelstrom.Message) error {
	return a.n.Reply(msg, map[string]string{
		"type": "generate_ok",
		"id":   ulid.Make().String(),
	})
}

func (a *App) handleBroadcastMessage(msg maelstrom.Message) error {
	body, err := readMessageBody(msg)
	if err != nil {
		return err
	}

	a.logPrintln(fmt.Sprintf("Handling broadcasting [From: %s] [To: %s] [Body: %s] ", msg.Src, msg.Dest, body))

	v, ok := body["message"].(float64)
	if !ok {
		a.logPrintln("invalid value")
		return fmt.Errorf("invalid value for message")
	}

	val := int64(v)

	a.msgMutex.Lock()
	if _, ok := a.messages[val]; ok {
		a.logPrintln("Message existed. Skip ...")
		a.msgMutex.Unlock()
	} else {
		a.messages[val] = true
		a.msgMutex.Unlock()
		go a.broadcast(msg)
	}

	a.logPrintln("finished handling msg for ", val)

	return a.n.Reply(msg, map[string]string{
		"type": "broadcast_ok",
	})
}

func (a *App) broadcast(originalMsg maelstrom.Message) {
	for _, node := range a.neighborNodes {
		if node == originalMsg.Src {
			continue
		}

		go func(nodeName string) {
			_ = a.n.Send(nodeName, originalMsg.Body)
			a.logPrintln("broadcasted message to n ", nodeName)
		}(node)
	}
}

func (a *App) handleReadMessage(msg maelstrom.Message) error {
	messages := make([]int64, len(a.messages))
	a.msgMutex.RLock()
	for k := range a.messages {
		messages = append(messages, k)
	}
	a.msgMutex.RUnlock()

	return a.n.Reply(msg, map[string]any{
		"type":     "read_ok",
		"messages": messages,
	})
}

func (a *App) handleTopologyMessage(msg maelstrom.Message) error {
	body, err := readMessageBody(msg)
	if err != nil {
		return err
	}

	var ok bool
	topo, ok := body["topology"].(map[string]any)
	if !ok {
		return errors.New("invalid topology")
	}

	values, existed := topo[a.n.ID()]

	if existed {
		rv := reflect.ValueOf(values)
		if rv.Kind() == reflect.Slice {
			ss := make([]string, rv.Len())

			for i := 0; i < rv.Len(); i++ {
				ss[i] = rv.Index(i).Interface().(string)
			}

			a.neighborNodes = ss
			a.logPrintln("Saved topology for current n: ", a.neighborNodes)
		}
	}

	return a.n.Reply(msg, map[string]string{
		"type": "topology_ok",
	})
}

func (a *App) setupLogFile() error {
	var err error
	a.logFile, err = openLogFile()
	if err != nil {
		return err
	}

	//defer func() {
	//	a.logFile.Sync()
	//	a.logFile.Close()
	//}()

	log.SetOutput(a.logFile)
	log.SetFlags(log.LstdFlags | log.Lshortfile | log.Lmicroseconds)
	return nil
}

func (a *App) logPrintln(args ...any) {
	msg := fmt.Sprintln(args...)
	log.Printf("[[%s]] %s", a.n.ID(), msg)
}

func openLogFile() (*os.File, error) {
	homeDir, _ := os.UserHomeDir()
	targetFilePath := fmt.Sprintf("%s/gg_run_%d.log", homeDir, time.Now().Unix())

	// abs, _ := filepath.Abs(targetFilePath)
	// fmt.Println(abs)

	f, err := os.OpenFile(targetFilePath, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0o644)
	if err != nil {
		return nil, err
	}

	return f, nil
}

func readMessageBody(msg maelstrom.Message) (map[string]any, error) {
	var body map[string]any

	if err := json.Unmarshal(msg.Body, &body); err != nil {
		return nil, err
	}

	return body, nil
}
